# Article-Repository

Repositories of all of my articles to push to my website

## Visuals
![Preview](https://gitlab.com/JoshMayBalGitHub/article-repository/-/raw/main/img/preview.png)
![Preview2](https://gitlab.com/JoshMayBalGitHub/article-repository/-/raw/main/img/preview2.png)

## License
Mozilla Public License v2.0

## Project status 
Rarely updated.
